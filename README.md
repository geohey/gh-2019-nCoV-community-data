# 2019-nCoV-community-data

## 新型冠状病毒相关数据

数据更新：每日10:00自动更新（每日不定时根据各地最新发布临时更新数据）

confirmed：确诊人数(-1未知）

坐标系：gcj02

数据源：
* [腾讯看点](https://ncov.html5.qq.com/community)
* [南都传媒](https://m.mp.oeeee.com/h5/pages/v20/nCovcase/)

数据类型：
* 日期.csv：小区列表
* 日期.geojson：小区位置数据
* 日期_line.geojson：确诊胡同、道路、街道等线状位置数据
* 日期_polygon.geojson：确诊小区边界
* hospital.csv：定点医院和发热门诊列表
* hospital.geojson：定点医院和发热门诊位置数据

数据预览：

![小区位置数据](https://images.gitee.com/uploads/images/2020/0207/000944_9071a580_384334.png "point1.png")
![胡同、道路、街道数据](https://images.gitee.com/uploads/images/2020/0207/001007_f5ed272c_384334.png "line1.png")
![小区边界数据](https://images.gitee.com/uploads/images/2020/0207/001023_0d610884_384334.png "polygon1.png")

